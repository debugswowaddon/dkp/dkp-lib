# dkp-lib


## Repo organization 

`lib/` contains the dkp lib that doesn't use any wow API


## Running tests and prepare code

Packages required:
 - `make`
 - `lua5.1`

Running `make` run tests and if all tests pass then the optimized lib code is in `lib/out`
