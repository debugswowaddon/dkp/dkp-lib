
.PHONY: all
all: build

.PHONY: build
build: amalg
	$(MAKE) -C lib/

.PHONY: amalg
amalg: /usr/local/bin/amalg.lua

/usr/local/bin/amalg.lua: ./3rd/lua-amalg-master/src/amalg.lua
	cp ./3rd/lua-amalg-master/src/amalg.lua /usr/local/bin
