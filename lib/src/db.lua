local base64 = require('base64')
local json = require('json')
local util = require('util')

db = {}

db.data = {}

-- Database definition:
--
-- players = []
-- -- playerID (like 622374381628162071)
-- -- -- name = string (discord name)
-- -- -- char = [] IG characters
-- -- -- -- name = string, (IG name)
-- -- -- -- isReroll = bool
-- -- -- dkp = {}
-- -- -- -- tier = []
-- -- -- -- -- dkp value for that tier
-- pu = []
-- -- name = {}
-- -- -- nbLoot = number

------------
-- encoding
------------
function db.b64decode(x)
	local status, res = pcall(function() return base64.decode(x) end)
	if status
	then
		return res
	else
		return nil
	end
end

function db.jsondecode(x)
	local status, res = pcall(function() return json.decode(x) end)
	if status
	then
		return res
	else
		return nil
	end
end

function db.b64encode(x)
	local status, res = pcall(function() return base64.encode(x) end)
	if status
	then
		return res
	else
		return nil
	end
end

function db.jsonencode(x)
	local status, res = pcall(function() return json.encode(x) end)
	if status
	then
		return res
	else
		return nil
	end
end


------------
-- database
------------
db.loadresult = {SUCCESS = 1, B64ERROR = 2, JSONERROR = 3}
function db.load(enc)
	local dataJson = db.b64decode(enc)
	if (dataJson == nil)
	then
		db.data = {}
		return db.loadresult.B64ERROR
	end
	db.data = db.jsondecode(dataJson)
	if (db.data == nil)
	then
		db.data = {}
		return db.loadresult.JSONERROR
	end
	return db.loadresult.SUCCESS
end

db.exportResult = {SUCCESS = 1, B64ERROR = 2, JSONERROR = 3}
function db.export()
	local dataJson = db.jsonencode(db.data)
	if (dataJson == nil)
	then
		return db.exportResult.JSONERROR
	end
	local str = db.b64encode(dataJson)
	if (str == nil)
	then
		return db.loadresult.B64ERROR
	end
	return db.exportResult.SUCCESS, str
end



------------------
-- DKP operations
------------------

db.addDKPToPlayerResult = {SUCCESS = 1, DB_ERROR = 2, PLAYER_NOT_FOUND = 3}
function db.addDKPToPlayer(amount, tier, playerID)
	local players = db.data.players
	if (players == nil)
	then
		return db.addDKPToPlayerResult.DB_ERROR
	end
	
	local player = players[playerID]
	if (player == nil)
	then
		return db.addDKPToPlayerResult.PLAYER_NOT_FOUND
	end

	local dkp = player.dkp
	if (dkp == nil or type(dkp) ~= "table")
	then
		player.dkp = {}
		dkp = player.dkp
	end

	if (dkp[tier] == nil)
	then
		dkp[tier] = 0
	end
	
	dkp[tier] = amount + dkp[tier] 

	return db.addDKPToPlayerResult.SUCCESS
end

db.setDKPToPlayerResult = {SUCCESS = 1, DB_ERROR = 2, PLAYER_NOT_FOUND = 3}
function db.setDKPToPlayer(amount, tier, playerID)
	local players = db.data.players
	if (players == nil)
	then
		return db.setDKPToPlayerResult.DB_ERROR
	end
	
	local player = players[playerID]
	if (player == nil)
	then
		return db.setDKPToPlayerResult.PLAYER_NOT_FOUND
	end

	local dkp = player.dkp
	if (dkp == nil or type(dkp) ~= "table")
	then
		player.dkp = {}
		dkp = player.dkp
	end

	if (dkp[tier] == nil)
	then
		dkp[tier] = 0
	end

	dkp[tier] = amount
	
	return db.setDKPToPlayerResult.SUCCESS
end


db.getPlayerDKPResult = {SUCCESS = 1, DB_ERROR = 2, PLAYER_NOT_FOUND = 3}
function db.getPlayerDKP(tier, playerID)
	local players = db.data.players
	if (players == nil)
	then
		return db.getPlayerDKPResult.DB_ERROR
	end
	
	local player = players[playerID]
	if (player == nil)
	then
		return db.getPlayerDKPResult.PLAYER_NOT_FOUND
	end

	local result, dkp = db.getDKP(tier, player)
	if (result == db.getDKPResult.DB_ERROR)
	then
		return db.getPlayerDKPResult.DB_ERROR
	end

	return db.getPlayerDKPResult.SUCCESS, dkp
end

db.getDKPResult = {SUCCESS = 1, DB_ERROR = 2}
function db.getDKP(tier, player)
	if (player == nil)
	then
		return db.getPlayerDKPResult.DB_ERROR
	end
	local dkp = player.dkp
	if (dkp == nil or type(dkp) ~= "table")
	then
		return db.getDKPResult.DB_ERROR
	end

	if (dkp[tier] == nil)
	then
		dkp[tier] = 0
	end
	
	return db.getDKPResult.SUCCESS, dkp[tier]
end


---------------------
-- player operations
---------------------

db.getPlayerWithNameResult = {SUCCESS = 1, DB_ERROR = 2, PLAYER_NOT_FOUND = 3}
function db.getPlayerWithName(playerName)
	local players = db.data.players
	if (players == nil)
	then
		return db.getPlayerWithNameResult.DB_ERROR
	end

	for playerID, player in pairs(players)
	do
		if (string.upper(player.name) == string.upper(playerName))
		then
			return db.getPlayerWithNameResult.SUCCESS, playerID, player
		end
	end

	return db.getPlayerWithNameResult.PLAYER_NOT_FOUND, nil, nil
end

db.getPlayerWithIGNameResult = {SUCCESS = 1, DB_ERROR = 2, PLAYER_NOT_FOUND = 3}
function db.getPlayerWithIGName(playerName)
	local players = db.data.players
	if (players == nil)
	then
		return db.getPlayerWithIGNameResult.DB_ERROR
	end

	for playerID, player in pairs(players)
	do
		if (player.char ~= nil)
		then
			for id, char in pairs(player.char)
			do
				if (char.name == playerName)
				then
					return db.getPlayerWithIGNameResult.SUCCESS, playerID, player
				end
			end

		end
	end

	return db.getPlayerWithIGNameResult.PLAYER_NOT_FOUND, nil, nil
end


db.addIGCharacterToPlayerResult = {SUCCESS = 1, DB_ERROR = 2, PLAYER_NOT_FOUND = 3, DUPLICATE = 4}
function db.addIGCharacterToPlayer(playerName, charName)
	local res, playerID, player = db.getPlayerWithIGName(charName)
	if (res ~= db.getPlayerWithIGNameResult.PLAYER_NOT_FOUND)
	then
		return db.addIGCharacterToPlayerResult.DUPLICATE
	end

	local res, playerID, player = db.getPlayerWithName(playerName)
	if (res ~= db.getPlayerWithNameResult.SUCCESS)
	then
		return res
	end

	if (player.char == nil)
	then
		player.char = {}
	end

	local newID = 0
	local id = 0
	while (player.char[id] ~= nil)
	do
		if (newID == id)
		then
			newID = newID + 1
		end
		id = id + 1
	end

	-- TODO: when a name is removed, newID can be zero but another char can be set as main already
	-- needs to iterate to find if there is a main already
	-- or just check if empty and assume it always need to have a main
	player.char[newID] = {name = charName, isMain = (newID == 0)}
	return db.addIGCharacterToPlayerResult.SUCCESS
end


db.addPUResult = {SUCCESS = 1, DUPLICATE = 4}
function db.addPU(name)
	if (db.data.pu == nil)
	then
		db.data.pu = {}
	end

	if (db.data.pu[name] ~= nil)
	then
		return db.addPUResult.DUPLICATE
	end

	db.data.pu[name] = {NBLoot = 0}
	return db.addPUResult.SUCCESS
end


function db.isPU(name)
	if (db.data.pu == nil)
	then
		db.data.pu = {}
	end

	for puname, pu in pairs(db.data.pu)
	do
		if (puname == name)
		then
			return true
		end
	end

	return false
end

db.giveLootToPUResult = {SUCCESS = 1, PLAYER_NOT_FOUND = 3}
function db.giveLootToPU(name, nb)
	if (nb == nil)
	then
		nb = 1
	end
	if (db.data.pu == nil)
	then
		db.data.pu = {}
	end

	for puname, pu in pairs(db.data.pu)
	do
		if (puname == name)
		then
			pu.NBLoot = pu.NBLoot + nb
			return db.giveLootToPUResult.SUCCESS
		end
	end

	return db.giveLootToPUResult.PLAYER_NOT_FOUND
end

db.getPUNBLootResult = {SUCCESS = 1, PLAYER_NOT_FOUND = 3}
function db.getPUNBLoot(name)
	if (db.data.pu == nil)
	then
		db.data.pu = {}
	end

	for puname, pu in pairs(db.data.pu)
	do
		if (puname == name)
		then
			return db.getPUNBLootResult.SUCCESS, pu.NBLoot
		end
	end

	return db.getPUNBLootResult.PLAYER_NOT_FOUND, nil

end


-----
return db
