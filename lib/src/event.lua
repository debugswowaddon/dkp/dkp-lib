local util = require('util')

event = {}

event.eventdb = {}
event.subs = {}

event.flags = 
{
	ERROR = 1,
	WARNING = 2,
	INFO = 3,
	DEBUG = 4,
}

event.topics =
{
	LOOT = 1,
	DKP = 2,
	CHAT = 3,
	DB = 4,
	STATE = 5,
}

function event.flagsToString(flag)
	local res = {}

	res[event.flags.ERROR] = "[EE]"
	res[event.flags.WARNING] = "[WW]"
	res[event.flags.INFO] = "[II]"
	res[event.flags.DEBUG] = "[DD]"

	return res[flag]
end

function event.topicsToString(topic)
	local res = {}

	res[event.topics.LOOT] = "{LOOT}"
	res[event.topics.DKP] = "{DKP}"
	res[event.topics.CHAT] = "{CHAT}"
	res[event.topics.DB] = "{DB}"
	res[event.topics.STATE] = "{STATE}"

	return res[topic]
end


-- db is optional: add it only if this event can be canceled
function event.onEvent(flag, topic, message, db)
	local dbCopy = nil
	local elem = nil
	if (db ~= nil)
	then
		dbCopy = util.deepCopy(db)
		elem = {flag = flag, topic = topic, cancel = false, message = message, db = dbCopy}
	else
		elem = {flag = flag, topic = topic, cancel = false, message = message}
	end
	table.insert(event.eventdb, elem)
	event.callSubs(elem)
	return nil
end

function event.callSubs(elem)
	for id, sub in pairs(event.subs)
	do
		sub(elem)
	end
end

-- cb takes one object arg
function event.subscribe(cb)
	table.insert(event.subs, cb)
end

return event
