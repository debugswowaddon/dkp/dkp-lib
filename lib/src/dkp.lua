local db = require('db')
local text = require('locale').text
local messageParser = require('message')
local util = require('util')
local event = require('event')

dkp = {}

dkp.states =
{
	OFF = 0, -- default mode
	STARTED = 1, -- raid is going but no loot are beeing bet on
	BID_P1 = 2, -- betting with +1
	BID_P5 = 3, -- betting with +5
	BID_P50 = 4, -- betting with +50
	BID_RAND = 5, -- betting with rand
	BID_RAND_PU = 6, -- betting with rand against PU
}
dkp.state = dkp.states.OFF

dkp.actions = 
{
	P1 = 0, -- player types +1
	P5 = 1, -- player types +5
	P50 = 2, -- player types +50
	RAND = 3, -- player uses /rand
}
dkp.actionOnState = {}

dkp.playerOnP1 = {}
dkp.playerOnP5 = {}
dkp.playerOnP50 = {}
dkp.randWinner = nil

function dkp.getDB()
	return db
end

function dkp.getEvent()
	return event
end

function dkp.addIGCharacterToPlayer(...)
	return db.addIGCharacterToPlayer(unpack(arg))
end

function dkp.stateToString(state)
	local switch = {}

	switch[dkp.states.OFF] = "OFF"
	switch[dkp.states.STARTED] = "STARTED"
	switch[dkp.states.BID_P1] = "BID +1"
	switch[dkp.states.BID_P5] = "BID +5"
	switch[dkp.states.BID_P50] = "BID +50"
	switch[dkp.states.BID_RAND] = "RAND"
	switch[dkp.states.BID_RAND_PU] = "RAND PU"

	return switch[state]
end

function dkp.changeState(state)
	local oldState = dkp.state
	dkp.state = state
	event.onEvent(event.flags.INFO, event.topics.STATE, "State changed from " .. dkp.stateToString(oldState) .. " to " .. dkp.stateToString(state))
end

function dkp.printEvent(elem)
	print(event.flagsToString(elem.flag), event.topicsToString(elem.topic), elem.message)
end

event.subscribe(dkp.printEvent)

function dkp.startRaid(encoded)
	if (dkp.state ~= dkp.states.OFF)
	then
		event.onEvent(event.flags.ERROR, event.topics.STATE, "Trying to start raid but not in OFF state. Current state is " .. dkp.stateToString(dkp.state))
		return nil, nil
	end
	local res = db.load(encoded)
	if (res == db.loadresult.SUCCESS)
	then
		dkp.changeState(dkp.states.STARTED)
	else
		event.onEvent(event.flags.ERROR, event.topics.DB, "Error loading database. Error code: " .. res)
	end
	return res
end

function dkp.endRaid()
	if (dkp.state ~= dkp.states.STARTED)
	then
		event.onEvent(event.flags.ERROR, event.topics.STATE, "Trying to stop raid but not in STARTED state. Current state is " .. dkp.stateToString(dkp.state))
		return nil, nil
	end
	local res, exported = db.export()
	if (res == db.exportResult.SUCCESS)
	then
		dkp.changeState(dkp.states.OFF)
	else
		event.onEvent(event.flags.ERROR, event.topics.DB, "Error exporting database. Error code: " .. res)
	end
	return res, exported
end

function dkp.startBid()
	if (dkp.state ~= dkp.states.STARTED)
	then
		event.onEvent(event.flags.ERROR, event.topics.STATE, "Trying to start bidding but not in STARTED state. Current state is " .. dkp.stateToString(dkp.state))
		return nil
	end

	dkp.playerOnP1 = {}
	dkp.playerOnP5 = {}
	dkp.playerOnP50 = {}
	dkp.randWinner = nil

	dkp.changeState(dkp.states.BID_P1)
	return nil
end

function dkp.onChatMessage(message, from)
	event.onEvent(event.flags.DEBUG, event.topics.CHAT, "Chat message from " .. from .. ": " .. message)
	if (dkp.state == dkp.states.OFF or dkp.state == dkp.states.STARTED)
	then
		return nil
	end
	local res, parsed = messageParser.parseRaidChat(message, from)
	if (res == messageParser.parseRaidChatResult.NOT_RELEVENT)
	then
		return nil
	end

	event.onEvent(event.flags.INFO, event.topics.CHAT, "Receive a +" .. parsed.message.P .. " from "  .. from .. " [" .. message .. "]")

	local action
	if (parsed.message.P == 1)
	then
		action = dkp.actions.P1
	end
	if (parsed.message.P == 5)
	then
		action = dkp.actions.P5
	end
	if (parsed.message.P == 50)
	then
		action = dkp.actions.P50
	end

	return dkp.onChatMessageAction(action, from)
end


----------------------
-- on event functions
----------------------

function dkp.onChatMessageAction(action, from)
	return dkp.actionOnState[action][dkp.state](from)
end


function dkp.buildOnMessageArray()
	local res = {}
	res[dkp.actions.P1] = dkp.onP1()
	res[dkp.actions.P5] = dkp.onP5()
	res[dkp.actions.P50] = dkp.onP50()
	-- TODO: res[dkp.actions.RAND] = dkp.onRAND()
	return res
end


---------
-- on p1
---------
function dkp.onP1()
	local res = {}
	res[dkp.states.BID_P1] = dkp.onP1P1
	res[dkp.states.BID_P5] = dkp.onP1P5
	res[dkp.states.BID_P50] = dkp.onP1P50
	res[dkp.states.BID_RAND] = dkp.onP1rand
	res[dkp.states.BID_RAND_PU] = dkp.onP1randpu
	return res
end

local tier = 0

function dkp.onP1P1(from)	
	if (db.isPU(from))
	then
		dkp.changeState(dkp.states.RAND_PU)
		return "pu need go rand"
	end

	local status, playerID, player = db.getPlayerWithIGName(from)
	if (status ~= db.getPlayerWithIGNameResult.SUCCESS)
	then
		return nil
	end

	local status, nbdkp = db.getDKP(tier, player)
	if (status ~= db.getDKPResult.SUCCESS)
	then
		return nil
	end

	if (nbdkp < 1)
	then
		return "error not enough dkp"
	end
	
	if (not util.isEmpty(dkp.playerOnP1))
	then
		dkp.changeState(dkp.states.BID_P5)
	end
	dkp.playerOnP1[from] = 1
	return nil
end

function dkp.onP1P5(from)
	if (db.isPU(from))
	then
		dkp.changeState(dkp.states.RAND_PU)
		return "pu need go rand"
	end

	return "error +1 during +5"
end

function dkp.onP1P50(from)
	if (db.isPU(from))
	then
		dkp.changeState(dkp.states.RAND_PU)
		return "pu need go rand"
	end
	return "error +1 during +50"
end

function dkp.onP1rand(from)
	if (db.isPU(from))
	then
		dkp.changeState(dkp.states.RAND_PU)
		return "pu need go rand"
	end
	return "error +1 during rand"
end

dkp.onP1randpu = dkp.onP1rand


---------
-- on p5
---------
function dkp.onP5()
	local res = {}
	res[dkp.states.BID_P1] = dkp.onP5P1
	res[dkp.states.BID_P5] = dkp.onP5P5
	res[dkp.states.BID_P50] = dkp.onP5P50
	res[dkp.states.BID_RAND] = dkp.onP5rand
	res[dkp.states.BID_RAND_PU] = dkp.onP5randpu
	return res
end


function dkp.onP5P1(from)
	return "error +5 during +1"
end

function dkp.onP5P5(from)
	if (db.isPU(from))
	then
		dkp.changeState(dkp.states.RAND_PU)
		return "pu need go rand"
	end

	local status, playerID, player = db.getPlayerWithIGName(from)
	if (status ~= db.getPlayerWithIGNameResult.SUCCESS)
	then
		return nil
	end

	local status, nbdkp = db.getDKP(tier, player)
	if (status ~= db.getDKPResult.SUCCESS)
	then
		return nil
	end

	if (nbdkp < 5)
	then
		return "error not enough dkp"
	end
	
	if (not util.isEmpty(dkp.playerOnP5))
	then
		dkp.changeState(dkp.states.BID_P50)
	end
	dkp.playerOnP5[from] = 1
	return nil
end

function dkp.onP5P50(from)
	return "error +5 during +50"
end

function dkp.onP5rand(from)
	return "error +5 during rand"
end

dkp.onP5randpu = dkp.onP5rand


---------
-- on p50
---------
function dkp.onP50()
	local res = {}
	res[dkp.states.BID_P1] = dkp.onP50P1
	res[dkp.states.BID_P5] = dkp.onP50P5
	res[dkp.states.BID_P50] = dkp.onP50P50
	res[dkp.states.BID_RAND] = dkp.onP50rand
	res[dkp.states.BID_RAND_PU] = dkp.onP50randpu
	return res
end


function dkp.onP50P1(from)
	return "error +50 during +1"
end

function dkp.onP50P5(from)
	return "error +50 during +5"
end

function dkp.onP50P50(from)
	if (db.isPU(from))
	then
		dkp.changeState(dkp.states.RAND_PU)
		return "pu need go rand"
	end

	local status, player = db.getPlayerWithIGName(from)
	if (status ~= db.getPlayerWithIGNameResult.SUCCESS)
	then
		return nil
	end

	local status, nbdkp = db.getDKP(tier, player)
	if (status ~= db.getDKPResult.SUCCESS)
	then
		return nil
	end

	if (nbdkp < 9)
	then
		return "error not enough dkp"
	end

	dkp.playerOnP50[from] = 1
	return nil
end

function dkp.onP50rand(from)
	return "error +50 during rand"
end

dkp.onP50randpu = dkp.onP50rand


dkp.actionOnState = dkp.buildOnMessageArray()

return dkp
