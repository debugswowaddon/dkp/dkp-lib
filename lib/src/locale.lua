locale = {}

locale.FRfr =
{
	["raid start welcome"] = "Le raid DKP commence",
	["start bet"] = "Debut des encheres pour ",
	["multiple +1 go to +5"] = "Il y a plusieurs +1, on passe en +5",
	["multiple +5 go to +50"] = "Il y a plusieurs +5, on passe en +50",
	["draw please rand"] = "Egalite, doivent /rand :",
	["error +1 during +5"] = "Il faut taper +5 et pas +1 dans cette phase",
	["error +1 during +50"] = "Il faut taper +50 et pas +1 dans cette phase",
	["error +1 during rand"] = "Il faut faire /rand et pas +1 dans cette phase",
	["error +5 during +1"] = "Il faut taper +1 et pas +5 dans cette phase",
	["error +5 during +50"] = "Il faut taper +50 et pas +5 dans cette phase",
	["error +5 during rand"] = "Il faut faire /rand et pas +5 dans cette phase",
	["error +50 during +1"] = "Il faut taper +1 et pas +50 dans cette phase",
	["error +50 during +5"] = "Il faut taper +5 et pas +50 dans cette phase",
	["error +50 during rand"] = "Il faut faire /rand et pas +50 dans cette phase",
	["error not enough dkp"] = "Pas assez de DKP a miser !",
	["pu need go rand"] = "Un PU est interesse, tous ceux qui veulent le loot font un /rand",
}

locale.text = locale.FRfr

return locale
