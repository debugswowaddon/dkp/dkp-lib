message = {}

-----------
-- parsing
-----------

message.parseRaidChatResult = {SUCCESS = 0, NOT_RELEVENT = 1}
function message.parseRaidChat(msg, from)
	if (msg == "+1")
	then
		return message.parseRaidChatResult.SUCCESS, {from = from, message = { P = 1} }
	end
	if (msg == "+5")
	then
		return message.parseRaidChatResult.SUCCESS, {from = from, message = { P = 5} }
	end
	if (msg == "+50")
	then
		return message.parseRaidChatResult.SUCCESS, {from = from, message = { P = 50} }
	end
	return message.parseRaidChatResult.NOT_RELEVENT
end

message.parseSystemChatResult = {SUCCESS = 0, NOT_RELEVENT = 1}
function message.parseSystemChat(msg)
	local iName = string.find(msg, " ")
	local name = string.sub(msg, 0, iName-1)

	local result = string.match(msg, "%d+")
	local range = string.match(msg, "%d+-%d+")
	
	if (result ~= nil)
	then
		result = tonumber(result)
	end

	if (name == nil or result == nil or range == nil)
	then
		return message.parseSystemChatResult.NOT_RELEVENT
	end

	if (range ~= "1-100")
	then
		return message.parseSystemChatResult.NOT_RELEVENT
	end

	return message.parseSystemChatResult.SUCCESS, {name = name, result = result}
end


return message
