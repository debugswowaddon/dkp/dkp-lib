local db = require('db')
local util = require('util')
local message = require('message')
local dkpManager = require('dkp')

assert(db ~= nil)
assert(util ~= nil)
assert(message ~= nil)
assert(dkpManager ~= nil)

local testjb64 = "eyJwbGF5ZXJzIjp7IjMyOTIwMTk4ODU2MDc0ODU0NSI6eyJuYW1lIjoiQmVyaHlsIiwiZGtwIjowfSwiNTY0NTQyODUzOTU1NTg0MDAwIjp7Im5hbWUiOiJbS0pdR2luZ2EvVGFla2V0Yy4uLiIsImRrcCI6MH0sIjIwNjMxNjIyMjQ1NzgzOTYxNiI6eyJuYW1lIjoic3RlbGlhbG9jayIsImRrcCI6MH0sIjIxNzYwNDIwNzk2Mjc1MDk3NyI6eyJuYW1lIjoiVmFoZWxlZW4iLCJka3AiOjB9LCI2MzA0NzcxMzM3NjgyOTQ0MjgiOnsibmFtZSI6Ikluw7xrdGFhaGwiLCJka3AiOjB9LCI0MzM1ODY3NDI4NzI3Njg1MjIiOnsibmFtZSI6InRoaWFzIiwiZGtwIjotMTAwMH0sIjY0NTk5MDg5MzgwMjg4MTA3NiI6eyJuYW1lIjoiUmFtZ2FsbC9SYWxpYXNzZSIsImRrcCI6MH0sIjI4MDQ0Njg4Nzk1MjI1MjkyOSI6eyJuYW1lIjoiW0tKXcOFcnTDqW3DrmEiLCJka3AiOjB9LCIyNDU5ODIzMTIzMDE0NjE1MDQiOnsibmFtZSI6IlN1c2UiLCJka3AiOjEwfSwiNjIyMzc0MzgxNjI4MTYyMDcxIjp7Im5hbWUiOiJFbGVuY3JvIiwiZGtwIjowfSwiMzc0NjE2MjY4MTQzMTk4MjA4Ijp7Im5hbWUiOiJNYWxseWNpYSIsImRrcCI6MH0sIjE5NjI2NTUxNDYyNzEwNDc2OCI6eyJuYW1lIjoiW0tKXU1ldWhnZWxsYW4iLCJka3AiOjB9LCI3MTgzNzA1OTMxMDc0NzY1NzAiOnsibmFtZSI6IkJvbm5lw6ByaWVuIiwiZGtwIjowfSwiMjU5NzgyODQ1Mzc0MDA1MjQ4Ijp7Im5hbWUiOiJUcmlmb3JjZSIsImRrcCI6MH0sIjI4MTU1NjEwMjcxMjEzMTU5NCI6eyJuYW1lIjoiW0tKXVNweWFuayIsImRrcCI6MH0sIjI3MjcxNjc3NTE0MDA5ODA0OCI6eyJuYW1lIjoiTm93YXkiLCJka3AiOjB9LCIxMzQ2MDk0MDgxODQ0MTgzMDQiOnsibmFtZSI6Im1lbGlhZHVzIiwiZGtwIjowfSwiMjcyNzU1Mzc4MzIyOTMxNzE1Ijp7Im5hbWUiOiJbS0pdTmFteiIsImRrcCI6MH0sIjg3MTQ2NzE2NTg5Nzg3MTQwMSI6eyJuYW1lIjoiTmVkcmlueXRoIiwiZGtwIjowfSwiMzMzNjA3NzE5NTk4NTU1MTM3Ijp7Im5hbWUiOiJbS0pdU2hyaXZlciIsImRrcCI6MH0sIjQwODcwODgzMzM3NzI1NTQzNSI6eyJuYW1lIjoiTGlsaGEiLCJka3AiOjB9LCIzMDU0MzQ5NDc1MTMwOTAwNDkiOnsibmFtZSI6InppemFjby9jYWNhdG9lcyIsImRrcCI6MH0sIjI4NTc2NjU1MDg5NDY3MzkyMSI6eyJuYW1lIjoiTsOpY3JvRG90IiwiZGtwIjowfSwiMzA0OTE5NzA5MjAxOTg5NjM0Ijp7Im5hbWUiOiJbS0pdw492YXJzIiwiZGtwIjowfX19"

--------------------------------
print("-- testing encoding --")
--------------------------------

local decoded = db.b64decode(testjb64)
assert(decoded ~= nil)
local parsed = db.jsondecode(decoded)
assert(parsed ~= nil)

assert(parsed["players"] ~= nil)
assert(parsed["players"]["245982312301461504"] ~= nil)
assert(parsed["players"]["245982312301461504"]["dkp"] == 10)

print("[pass]", "Decoding jb64 message")

decoded = db.jsonencode(parsed)
assert(decoded ~= nil)
local recovered = db.b64encode(decoded)
assert(recovered ~= nil)

print("[pass]", "Encoding jb64 message")

----------------------------------
print("-- testing db loading --")
----------------------------------

local res = db.load(testjb64)
assert(res == db.loadresult.SUCCESS)
assert(db.data ~= nil)
assert(db.data["players"] ~= nil)
assert(db.data["players"]["245982312301461504"] ~= nil)
assert(db.data["players"]["245982312301461504"]["dkp"] == 10)

print("[pass]", "Loading database succcess")


local res = db.load("pppppp") --invalid b64
assert(res == db.loadresult.B64ERROR)
assert(db.data["players"] == nil)

print("[pass]", "Loading database b64 error")

local res = db.load("VGhpYXMgZXN0IHVuIGZpb24=") -- valid b64, invalid json
assert(res == db.loadresult.JSONERROR)
assert(db.data["players"] == nil)

print("[pass]", "Loading database json error")

local res = db.load(testjb64)
assert(res == db.loadresult.SUCCESS)
local status, res = db.export()
assert(status == db.exportResult.SUCCESS)
assert(res ~= nil)
assert(res ~= "")
print("[pass]", "Exporting database")


--------------------------------------------
print("-- testing DKP operations on db --")
--------------------------------------------

local res = db.load(testjb64)
assert(res == db.loadresult.SUCCESS)

-- TODO: update DB bj64 to the newer db format with tiers
local res = db.setDKPToPlayer(0, 1, "622374381628162071")
assert(res == db.setDKPToPlayerResult.SUCCESS)
print("[pass]", "Setting DKP from player ID")

local status, dkp = db.getPlayerDKP(1, "622374381628162071")
assert(status == db.getPlayerDKPResult.SUCCESS)
assert(dkp == 0)
print("[pass]", "Getting DKP from player ID")
local res = db.addDKPToPlayer(10, 1, "622374381628162071")
assert(res == db.addDKPToPlayerResult.SUCCESS)
local status, dkp = db.getPlayerDKP(1, "622374381628162071")
assert(status == db.getPlayerDKPResult.SUCCESS)
assert(dkp == 10)
print("[pass]", "Adding DKP to player ID")

local status, dkp = db.getPlayerDKP(1, "wrong id")
assert(status == db.getPlayerDKPResult.PLAYER_NOT_FOUND)
print("[pass]", "Getting DKP from player ID with wrong id")
local res = db.addDKPToPlayer(10, 1, "wrong id")
assert(res == db.addDKPToPlayerResult.PLAYER_NOT_FOUND)
print("[pass]", "Adding DKP to player ID with wrong id")


-----------------------------------------------
print("-- testing player operations on db --")
-----------------------------------------------

local status, playerID, player = db.getPlayerWithName("Suse")
assert(status == db.getPlayerWithNameResult.SUCCESS)
assert(playerID == "245982312301461504")
assert(player ~= nil)
assert(player.name == "Suse")
print("[pass]", "Getting player with name")

local status, playerID, player = db.getPlayerWithName("sUSe")
assert(status == db.getPlayerWithNameResult.SUCCESS)
assert(playerID == "245982312301461504")
assert(player ~= nil)
assert(player.name == "Suse")
print("[pass]", "Getting player with case insensitive name")

local status, playerID, player = db.getPlayerWithName("invalid name here")
assert(status == db.getPlayerWithNameResult.PLAYER_NOT_FOUND)
assert(playerID == nil)
assert(player == nil)
print("[pass]", "Getting player with invalid name")

local status, playerID, player = db.getPlayerWithIGName("Suse")
assert(status == db.getPlayerWithNameResult.PLAYER_NOT_FOUND)
assert(playerID == nil)
assert(player == nil)
print("[pass]", "Getting player character that is not registered yet")

local status = db.addIGCharacterToPlayer("Suse", "Suse")
assert(status == db.addIGCharacterToPlayerResult.SUCCESS)
local status, playerID, player = db.getPlayerWithIGName("Suse")
assert(status == db.getPlayerWithNameResult.SUCCESS)
assert(playerID == "245982312301461504")
assert(player ~= nil)
assert(player.name == "Suse")
print("[pass]", "Adding a character to a player")

local status = db.addIGCharacterToPlayer("Suse", "Suse")
assert(status == db.addIGCharacterToPlayerResult.DUPLICATE)
print("[pass]", "Adding duplicate character to a player")

local status = db.addPU("buhbuh")
assert(status == db.addPUResult.SUCCESS)
print("[pass]", "Adding a PU")

local status = db.addPU("buhbuh")
assert(status == db.addPUResult.DUPLICATE)
print("[pass]", "Adding a PU duplicate")

local status = db.giveLootToPU("buhbuh")
assert(status == db.giveLootToPUResult.SUCCESS)
print("[pass]", "Giving a loot to a PU")

local status = db.giveLootToPU("player that doesn't exist")
assert(status == db.giveLootToPUResult.PLAYER_NOT_FOUND)
print("[pass]", "Giving a loot to a PU that doesn't exist")

local status, nb = db.getPUNBLoot("buhbuh")
assert(status == db.getPUNBLootResult.SUCCESS)
assert(nb == 1)
print("[pass]", "Get PU NB loot")

local status, nb = db.getPUNBLoot("player that doesn't exist")
assert(status == db.getPUNBLootResult.PLAYER_NOT_FOUND)
print("[pass]", "Get PU NB loot player that doesn't exist")


---------------------------------------
print("-- testing message parsing --")
---------------------------------------

local status, res = message.parseSystemChat("test rolls 5 (1-100)")
assert(status == message.parseSystemChatResult.SUCCESS)
assert(res.name == "test")
assert(res.result == 5)
print("[pass]", "Parse ENen roll")

local status, res = message.parseSystemChat("test rolls 5 (1-1000)")
assert(status == message.parseSystemChatResult.NOT_RELEVENT)
assert(res == nil)
print("[pass]", "Parse ENen roll 1-1000")

local status, res = message.parseSystemChat("invalid message")
assert(status == message.parseSystemChatResult.NOT_RELEVENT)
assert(res == nil)
print("[pass]", "Parse invalid roll")


local status, res = message.parseRaidChat("+1", "test")
assert(status == message.parseRaidChatResult.SUCCESS)
assert(res ~= nil)
assert(res.from == "test")
assert(res.message ~= nil)
assert(res.message.P ~= nil)
assert(res.message.P == 1)
print("[pass]", "Parse raid chat +1")

local status, res = message.parseRaidChat("+5", "test")
assert(status == message.parseRaidChatResult.SUCCESS)
assert(res ~= nil)
assert(res.from == "test")
assert(res.message ~= nil)
assert(res.message.P ~= nil)
assert(res.message.P == 5)
print("[pass]", "Parse raid chat +5")

local status, res = message.parseRaidChat("+50", "test")
assert(status == message.parseRaidChatResult.SUCCESS)
assert(res ~= nil)
assert(res.from == "test")
assert(res.message ~= nil)
assert(res.message.P ~= nil)
assert(res.message.P == 50)
print("[pass]", "Parse raid chat +50")

local status, res = message.parseRaidChat("other message", "test")
assert(status == message.parseRaidChatResult.NOT_RELEVENT)
assert(res == nil)
print("[pass]", "Parse raid chat not +something")


-------------------------------------
print("-- testing state machine --")
-------------------------------------

assert(dkpManager.state == dkpManager.states.OFF)
local res = dkpManager.startRaid(testjb64)
assert(res == db.loadresult.SUCCESS)
assert(dkpManager.state == dkpManager.states.STARTED)
print("[pass]", "Starting raid")


local res = dkpManager.startRaid(testjb64)
assert(res == nil)
assert(dkpManager.state == dkpManager.states.STARTED)
print("[pass]", "Starting raid in wrong state")

local res, exported = dkpManager.endRaid()
assert(res == db.loadresult.SUCCESS)
assert(exported ~= nil)
assert(dkpManager.state == dkpManager.states.OFF)
print("[pass]", "Ending raid")

local res, exported = dkpManager.endRaid()
assert(res == nil)
assert(exported == nil)
assert(dkpManager.state == dkpManager.states.OFF)
print("[pass]", "Ending raid in wrong state")


assert(dkpManager.state == dkpManager.states.OFF)
local res = dkpManager.startRaid(testjb64)
assert(res == db.loadresult.SUCCESS)
assert(dkpManager.state == dkpManager.states.STARTED)


local status = dkpManager.getDB().addDKPToPlayer(10, 0, "245982312301461504")
assert(status == db.addDKPToPlayerResult.SUCCESS)
local status = dkpManager.addIGCharacterToPlayer("Suse", "player1")
assert(status == db.addIGCharacterToPlayerResult.SUCCESS)
local status = dkpManager.addIGCharacterToPlayer("Suse", "player2")
assert(status == db.addIGCharacterToPlayerResult.SUCCESS)


local res = dkpManager.startBid()
assert(res == nil)
assert(dkpManager.state == dkpManager.states.BID_P1)
print("[pass]", "Start bidding")

local res = dkpManager.onChatMessage("+1", "player1")
assert(res == nil)
assert(dkpManager.state == dkpManager.states.BID_P1)
print("[pass]", "+1")

local res = dkpManager.onChatMessage("+1", "player2")
assert(res == nil)
assert(dkpManager.state == dkpManager.states.BID_P5)
print("[pass]", "2nd +1")

local res = dkpManager.onChatMessage("+5", "player1")
assert(res == nil)
assert(dkpManager.state == dkpManager.states.BID_P5)
print("[pass]", "+5")

local res = dkpManager.onChatMessage("+5", "player2")
assert(res == nil)
assert(dkpManager.state == dkpManager.states.BID_P50)
print("[pass]", "2nd +5")
